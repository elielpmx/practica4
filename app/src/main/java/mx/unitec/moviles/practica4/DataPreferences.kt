package mx.unitec.moviles.practica4

import android.content.Context
import android.preference.PreferenceManager

private const val PREF_RFC = "dataRFC"

object DataPreferences {

    fun getStoredRFC(context: Context): String{
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(PREF_RFC,  "")!!

    }

    fun setStoredRFC(context: Context, query: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(PREF_RFC, query)
            .apply()


    }
}